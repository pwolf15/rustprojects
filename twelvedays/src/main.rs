fn main() {

    let ordinals = [ 
        "first",
        "second",
        "third",
        "fourth",
        "fifth",
        "sixth",
        "seventh",
        "eighth",
        "ninth",
        "tenth",
        "eleventh",
        "twelfth"
    ];
    
    let gifts = [
        "A partridge in a pear tree",
        "Two turtle doves",
        "Three frech hens",
        "Four calling birds",
        "Five gold rings",
        "Six geese a laying",
        "Seven swans a swimming",
        "Eight maids a milking",
        "Nine ladies dancing",
        "Ten lords a leaping",
        "Eleven pipers piping",
        "Twelve drummers drumming"
    ];

    let mut i: usize = 1;
    for ordinal in ordinals.iter() {
        println!("On the {} day of Christmas", ordinal);
        println!("My true love gave to me,");

        for ele in (0..i).rev() {
            if ele > 0 {
                println!("{},", gifts[ele]);
            } else {
                if i > 1 {
                    println!("And a partridge in a pear tree.");
                } else {
                    println!("A partridge in a pear tree.");
                }    
            } 
        }

        println!("\n");
        i += 1;
    }
}
