fn main() {
    // let s1 = String::from("hello");
    // let s2 = s1;

    // println!("{}, world!", s1);

    let s1 = String::from("hello");
    let s2 = s1.clone();

    println!("s1 = {}, s2 = {}", s1, s2);

    let s = String::from("hello");

    takes_ownership(s);

    let x = 5;

    makes_copy(x);

    let s1 = gives_ownership();

    let mut s2 = String::from("hello");

    let mut s3 = takes_and_gives_back(s2);

    println!("{}", s3);

    let mut s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);

    change(&mut s1);

    {
        let r1 = &mut s1;
    }

    let r4 = &mut s3;

    let reference_to_something = no_dangle();
    println!("{}", reference_to_something);

    let s = String::from("hello world");

    let hello = &s[0..5];
    let world = &s[6..11];
    let slice = &s[..2];
    let slice = &s[3..];

    let mut s = String::from("hello world");
    let word = first_word(&s);
    // s.clear();
    println!("{}", word);

    let my_string_literal = "hello world";

    let word = first_word(my_string_literal);
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

// fn calculate_length(s: String) -> (String, usize) {
//     let length = s.len();

//     (s, length)
// }

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn gives_ownership() -> String {

    let some_string = String::from("hello");

    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

fn no_dangle() -> String {
    let s = String::from("hello");

    s
}

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' '{
            return &s[0..i]
        }
    }

    &s[..]
}