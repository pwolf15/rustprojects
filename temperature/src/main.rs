use std::io;
use std::cmp::Ordering;

fn main() {

    loop {
        println!("Convert to Fahrenheit or Celsius (F/C)?");

        let mut convert_to_scale = String::new();
    
        io::stdin().read_line(&mut convert_to_scale)
            .expect("Failed to read scale");
    
        let convert_to_scale: String = match convert_to_scale.trim().parse() {
            Ok(scale) => scale,
            Err(_) => continue,
        };

        println!("{}", convert_to_scale);

        if convert_to_scale == "C" {
            println!("Converting to Celsisus");
        } else if convert_to_scale == "F" {
            println!("Converting to Fahrenheit");
        } else {
            println!("Unrecognized option!");
            continue;
        }

        println!("Enter a value!");

        let mut value = String::new();
        io::stdin().read_line(&mut value)
            .expect("Failed to read value");

        let value: f32 = match value.trim().parse() {
            Ok(value) => value,
            Err(_) => continue,
        };

        println!("{}", value);

        if convert_to_scale == "C" {
            println!("Celsius {} => Fahrenheit {}", value, get_fahrenheit(value));
        } else if convert_to_scale == "F" {
            println!("Fahrenheit {} => Celsius {}", value, get_celsius(value));
        }
    }
}

fn get_fahrenheit(temp: f32) -> f32 {
    return (9.0/5.0) * temp + 32.0;
}

fn get_celsius(temp: f32) -> f32 {
    return (5.0/9.0) * (temp - 32.0);
}
