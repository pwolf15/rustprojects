use std::io;

fn main() {

    loop {
        println!("Enter a number.");

        let mut n = String::new();

        io::stdin().read_line(&mut n)
            .expect("Failed to read number");

        let n: i32 = match n.trim().parse() {
            Ok(n) => n,
            Err(_) => continue,
        };

        if n < 0 {
            println!("Can't compute fibonacci for negatives.");
            continue;
        }

        println!("Fibonacci for {}: {}", n, fibonacci(n));
    }
}

fn fibonacci(n: i32) -> i32 {

    if n == 0 {
        return 0
    } else if n == 1 || n == 2 {
        return 1
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
